{
    "AWSTemplateFormatVersion" : "2010-09-09",
    "Description"              : "WebServer for czerwoni.pl and matkids, problog, tenis supporting LAMP and ASP.NET Core",
    "Parameters"               : {
        "InstanceType" : {
            "Description" : "WebServer EC2 instance type",
            "Type"        : "String",
            "Default"     : "t2.micro",
            "AllowedValues" : ["t2.micro","t2.small","t2.medium"],
            "ConstraintDescription" : "must be a valid EC2 instance type."
        },
        "KeyName"      : {
            "Description" : "Name of an EC2 KeyPair to enable SSH access to the instance.",
            "Type"        : "AWS::EC2::KeyPair::KeyName",
            "ConstraintDescription" : "must be the name of an existing EC2 KeyPair."
        }
    },
    "Mappings"                 : {
        "AWSInstanceType2Arch" : {
            "t2.micro" : {"Arch" : "HVM64"},
            "t2.small" : {"Arch" : "HVM64"},
            "t2.medium" : {"Arch" : "HVM64"}
        },
        "AWSInstanceType2NATArch" : {
            "t2.micro" : {"Arch" : "NATHVM64"},
            "t2.small" : {"Arch" : "NATHVM64"},
            "t2.medium" : {"Arch" : "NATHVM64"}
        },
        "AWSRegionArch2AMI"       : {
            "eu-central-1" : {
                "HVM64" : "ami-af79ebc0"
            }
        }
    },
    "Resources"                : {
        "CzerwoniEC2"     : {
            "Type" : "AWS::EC2::Instance",
            "Metadata": {
                "AWS::CloudFormation::Init" : {
                    "configSets" : {
                        "InstallAndRun" : [ "Install" ]
                    },
                    "Install" : {
                        "files" : {
                            "/etc/cfn/cfn-hup.conf" : {
                                "content" : { "Fn::Join" : ["", [
                                    "[main]\n",
                                    "stack=", { "Ref" : "AWS::StackId" }, "\n",
                                    "region=", { "Ref" : "AWS::Region" }, "\n"
                                ]]},
                                "mode"    : "000400",
                                "owner"   : "root",
                                "group"   : "root"
                            },
                            "/etc/cfn/hooks.d/cfn-auto-reloader.conf" : {
                                "content": { "Fn::Join" : ["", [
                                    "[cfn-auto-reloader-hook]\n",
                                    "triggers=post.update\n",
                                    "path=Resources.CzerwoniEC2.Metadata.AWS::CloudFormation::Init\n",
                                    "action=/usr/local/bin/cfn-init -v ",
                                    "         --stack ", { "Ref" : "AWS::StackName" },
                                    "         --resource CzerwoniEC2 ",
                                    "         --configsets InstallAndRun ",
                                    "         --region ", { "Ref" : "AWS::Region" }, "\n",
                                    "runas=root\n"
                                ]]}
                            }
                        },
                        "services" : {
                            "sysvinit" : {
                                "cfn-hup" : { "enabled" : "true", "ensureRunning" : "true",
                                    "files" : ["/etc/cfn/cfn-hup.conf", "/etc/cfn/hooks.d/cfn-auto-reloader.conf"]}
                                }
                        }
                    }
                }
            },
            "Properties" : {
                    "UserData" : {
                        "Fn::Base64" : {"Fn::Join" : ["",[
                            "#!/bin/bash\n",
                            "set -o errexit; set -o nounset; set -o pipefail\n",
                            "# Install AWS cfn-bootstrap utilities\n",
                            "apt-get update\n",
                            "apt-get -y install python-pip\n",
                            "pip install https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz\n",
                            "cp /usr/local/init/ubuntu/cfn-hup /etc/init.d/cfn-hup \n",
                            "chmod +x /etc/init.d/cfn-hup \n",
                            "update-rc.d cfn-hup defaults \n ",
                            "service cfn-hup start \n",
                            "/usr/local/bin/cfn-init -v ", " --stack ", { "Ref": "AWS::StackName" }, " --resource CzerwoniEC2", " --configsets InstallAndRun ", " --region ", { "Ref": "AWS::Region" }, "\n",
                            "/usr/local/bin/cfn-signal -e $? ", " --stack ", { "Ref" : "AWS::StackName" }, " --resource CzerwoniEC2", " --region ", { "Ref" : "AWS::Region" }, "\n"
                        ]]}
                    },
                    "InstanceType" : {"Ref" : "InstanceType"},
                    "SecurityGroups" : [{"Ref" : "CzerwoniSecurityGroup"}],
                    "KeyName"        : {"Ref" : "KeyName"},
                    "ImageId"        : {
                        "Fn::FindInMap" : ["AWSRegionArch2AMI",{"Ref" : "AWS::Region"},{"Fn::FindInMap" : ["AWSInstanceType2Arch",{"Ref" : "InstanceType"},"Arch"]}]
                    }
                }
        },
        "CzerwoniSecurityGroup" : {
            "Type" : "AWS::EC2::SecurityGroup",
            "Properties" : {
                "GroupDescription" : "Enable SSH access",
                "SecurityGroupIngress" : [
                    {"IpProtocol" : "tcp", "FromPort" : "22","ToPort" : "22","CidrIp" : "0.0.0.0/0"},
					{"IpProtocol" : "tcp", "FromPort" : "80", "ToPort" : "80", "CidrIp" : "0.0.0.0/0"}
                ]
            }
        },
        "CzerwoniEIPAssociation"               : {
            "Type" : "AWS::EC2::EIPAssociation",
            "Properties" : {
                "InstanceId" : {"Ref" : "CzerwoniEC2"},
                "EIP"        : {"Fn::ImportValue" : "CzerwoniEIP" }
            }
        }
    },
    "Outputs"                  : {
        "InstanceId" : {
            "Description" : "InstanceId of the newly created EC2 instance",
            "Value"       : {"Ref" : "CzerwoniEC2"}
        },
		"WebsiteURL" : {
			"Description" : "URL for newly created web stack",
			"Value" : { "Fn::Join" : ["", ["http://", { "Fn::GetAtt" : [ "CzerwoniEC2", "PublicDnsName" ]}]] }
        },
        "IP" : {
            "Value" : {"Fn::ImportValue" : "CzerwoniEIP" },
			"Export" : { "Name" : "CzerwoniIP" }
        }
    }
}